import { Engine } from "@babylonjs/core/Engines/engine";
import { Overlay } from "./Overlay";
import { PerfOverlay } from "./PerfOverlay";


export type TerminalCommand = {
    name: string
    type: BooleanConstructor | NumberConstructor
    action: (this: Terminal, value: any) => void
}

export class Terminal {

    // Static

    private static _commands: TerminalCommand[] = [];
    public static get commands(): TerminalCommand[] {
        return [

        ];
    }

    public static RegisterTerminalCommand(command: TerminalCommand): void {
        this._commands.push(command);
    }


    // Local

    private _engine: Engine;
    private _target: HTMLElement;

    public get engine(): Engine {
        return this._engine;
    }
    public perfOverlay: PerfOverlay;

    constructor(engine: Engine, overlayTarget: HTMLElement) {
        this._engine = engine;
        this._target = overlayTarget;
    }

}