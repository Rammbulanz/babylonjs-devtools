import { Engine } from "@babylonjs/core/Engines/engine";
import { Viewport } from "@babylonjs/core/Maths/math";

export abstract class Overlay {

    private _engine: Engine;
    private _canvas: HTMLCanvasElement;

    /**
     * Describes the section of the screen, where this overlay should be placed
     */
    public section: Viewport;

    public get engine(): Engine {
        return this._engine;
    }


    constructor(engine: Engine) {
        this._engine = engine;
    }

    /**
     * Render the overlay onto the canvas context
     */
    public abstract render(): void;

}