/**
 * Base on EA Frostbite Engine PerfOverlay
 */

import { Engine } from '@babylonjs/core/Engines/engine';
import { Overlay } from './Overlay';
import { Terminal } from './Terminal';

export class PerfOverlay extends Overlay {

    private _drawFps: boolean = false;
    private _drawGraph: boolean = false;


    public get drawFps(): boolean {
        return this._drawFps;
    }
    public set drawFps(value: boolean) {
        this._drawFps = true;
        // ToDO - Initialize drawing
    }

    public get drawGraph(): boolean {
        return this._drawGraph;
    }


    constructor(engine: Engine) {
        super(engine);
    }

    public render(): void {
        // ToDo
    }

}

Terminal.RegisterTerminalCommand({
    name: 'PerfOverlay.drawFps',
    type: Boolean,
    action: function (this: Terminal, value: boolean) {
        if (!this.perfOverlay) {
            this.perfOverlay = new PerfOverlay(this.engine);
        }

        this.perfOverlay.drawFps = value;
    }
})